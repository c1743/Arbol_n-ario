/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolnario;

/**
 *
 * @author AndresFWilT
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

public class interfaz extends JFrame {

    public int flag;
    public int WinWidth = 1450;
    public int WinHeight = 900;
    public int LROffset = 170;
    public int DownOffset = 50;
    public int nodeD = 26;
    public int levelOffset = 50;
    public JPanel panel;
    public JPanel lienzo;
    public JPanel info;
    public JLabel Titulo;
    public JButton Ingresar;
    public JButton Consultar;
    public JButton Cerrar;
    public JButton Diccionario;
    public JButton Recorridos;
    public JButton borrar;
    public JLabel labelPalabra;
    public JLabel labelTraduccion;
    public JLabel labelBuscar;
    public JLabel lPre, lPos, lIn;
    public JLabel arbol;
    public JTextField IngresarPalabra;
    public JTextField IngresarTraduccion;
    public JTextField IngresarBuscar;
    public Graphics graphics;
    public Graphics2D g;
    public logica ArbolE;
    public JTable tableDiccionario;
    public TableRowSorter tr;// = new TableRowSorter<DefaultTableModel>(modelo);
    DefaultTableModel modelo = new DefaultTableModel();
    TableColumn columnaABorrar;
    public JTable tabla = new JTable(modelo);
    public JTable aux = new JTable(modelo);
    JScrollPane scroll1 = new JScrollPane(tabla);
    public ArrayList<String> pre;
    public ArrayList<String> pos;

    public interfaz() {
        createTable();
        scroll1.setVisible(false);

        this.ArbolE = new logica();
        this.setTitle("Arbol N-ario");
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setLocation(0, 0);
        this.setSize(WinWidth, WinHeight);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        info = new JPanel();
        info.setBounds(750, 400, 450, 450);
        info.setLayout(null);
        this.getContentPane().add(info);

        panel = new JPanel();
        panel.setBounds(0, 0, 900, 150);
        System.out.println((int) (this.WinWidth * 0.40));
        panel.setLayout(null);
        this.getContentPane().add(panel);

        lienzo = new JPanel();
        lienzo.setBounds(20, 160, 700, 700);
        lienzo.setAutoscrolls(true);
        this.getContentPane().add(lienzo);

        labelPalabra = new JLabel("Palabra");
        labelPalabra.setBounds(5, 5, 50, 50);

        arbol = new JLabel("GRAFO");
        arbol.setBounds(320, 116, 50, 50);
        arbol.setVisible(true);
        arbol.setForeground(Color.RED);
        panel.add(arbol);

        panel.add(labelPalabra);

        labelTraduccion = new JLabel("Traduccion");
        labelTraduccion.setBounds(5, 40, 80, 50);
        //labelPalabraTraducida.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(labelTraduccion);

        labelBuscar = new JLabel("Buscar");
        labelBuscar.setBounds(600, 10, 50, 20);
        labelBuscar.setVisible(false);
        panel.add(labelBuscar);

        IngresarPalabra = new JTextField();
        IngresarPalabra.setBounds(80, 20, 100, 20);
        IngresarPalabra.setVisible(true);
        panel.add(IngresarPalabra);

        IngresarTraduccion = new JTextField();
        IngresarTraduccion.setBounds(80, 55, 100, 20);
        panel.add(IngresarTraduccion);

        IngresarBuscar = new JTextField();
        IngresarBuscar.setBounds(600, 10, 100, 20);
        IngresarBuscar.setVisible(false);
        IngresarBuscar.addKeyListener(new KeyListener() {
            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
                int key = e.getKeyCode();
                if (e.getKeyChar() == '\n') {
                    String Palabre = IngresarBuscar.getText();
                    filtrar(Palabre);
                    System.out.println("Enter");
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }
        });
        panel.add(IngresarBuscar);

        Diccionario = new JButton("Diccionario");
        Diccionario.setBounds(250, 55, 100, 20);
        Diccionario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Recorridos.setVisible(true);
                Cerrar.setVisible(true);
                scroll1.setVisible(true);
            }
        });
        panel.add(Diccionario);

        borrar = new JButton("Borrar palabra");
        borrar.setBounds(250, 100, 150, 20);
        borrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean palabraExiste = false;
                for (int i = 0; i < tabla.getRowCount(); i++) {
                    if (tabla.getValueAt(i, 0).toString().equals(IngresarPalabra.getText())) {
                        System.out.println("La palabra ya existe");
                        palabraExiste = true;
                    }
                }
                if (palabraExiste == true) {
                    for (int i = 0; i < tabla.getRowCount(); i++) {
                        if (tabla.getValueAt(i, 0).toString().equals(IngresarPalabra.getText())) {
                            modelo.removeRow(i);
                        } else {
                            aux.setValueAt(tabla.getValueAt(i, 0), i, 0);
                        }
                    }
                    logica borrarP = new logica();
                    try {
                        for (int i = 0; i < aux.getRowCount(); i++) {
                            borrarP.addPalabra(String.valueOf(tabla.getValueAt(i, 0)), String.valueOf(tabla.getValueAt(i, 1)));
                            String[] fila = {
                                String.valueOf(tabla.getValueAt(i, 0)),
                                String.valueOf(tabla.getValueAt(i, 1))};

                            if ("".equals(String.valueOf(tabla.getValueAt(i, 0)))) {

                            }
                        }
                        IngresarPalabra.setText("");
                        IngresarTraduccion.setText("");
                        ArbolE = borrarP;
                        repintar(ArbolE);
                    } catch (Exception error) {

                    }
                    JOptionPane.showMessageDialog(null, "Palabra borrada.");
                } else {
                    JOptionPane.showMessageDialog(null, "La palabra '" + IngresarPalabra.getText() + "' no existe.");
                }
            }
        });
        panel.add(borrar);

        Recorridos = new JButton("Orden");
        Recorridos.setBounds(370, 55, 100, 20);
        Recorridos.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String in = "";
                ArbolE.inorden(ArbolE.getRaiz());
                for (int i = 0; i < ArbolE.getIn().size(); i++) {
                    in = in + ArbolE.getIn().get(i);
                }
                String pos = "";
                ArbolE.posorden(ArbolE.getRaiz());
                for (int i = 0; i < ArbolE.getPos().size(); i++) {
                    pos = pos + ArbolE.getPos().get(i);
                }
                String pre = "";
                ArbolE.preorden(ArbolE.getRaiz());
                for (int i = 0; i < ArbolE.getPre().size(); i++) {
                    pre = pre + ArbolE.getPre().get(i);
                }
                lPre.setVisible(true);
                lPos.setVisible(true);
                lIn.setVisible(true);
                lPre.setText("Pre: " + pre);
                lPos.setText("Pos: " + pos);
                lIn.setText("In: " + in);

                ArbolE.limpiarIn();
                ArbolE.limpiarPos();
                ArbolE.limpiarPre();
                /*repintar(ArbolE);
                panel.repaint();*/
            }

        });
        panel.add(Recorridos);
        Cerrar = new JButton("X");
        Cerrar.setBounds(500, 35, 50, 20);
        Cerrar.setBackground(Color.red);
        Cerrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarPalabra.setVisible(true);
                labelPalabra.setVisible(true);
                IngresarTraduccion.setVisible(true);
                labelTraduccion.setVisible(true);
                IngresarBuscar.setVisible(false);
                labelBuscar.setVisible(false);
                scroll1.setVisible(false);
                Cerrar.setVisible(false);
                Recorridos.setVisible(true);

            }
        });
        panel.add(Cerrar);
        Cerrar.setVisible(false);

        Consultar = new JButton("Buscar");
        Consultar.setBounds(370, 20, 100, 20);
        Consultar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarBuscar.setVisible(true);
                labelBuscar.setVisible(true);
                scroll1.setVisible(true);
                Cerrar.setVisible(true);
            }
        });
        panel.add(Consultar);
        //Boton ingresar
        Ingresar = new JButton("Ingresar");
        Ingresar.setBounds(250, 20, 100, 20);
        Ingresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean palabraExiste = false;
                for (int i = 0; i < tabla.getRowCount(); i++) {
                    if (tabla.getValueAt(i, 0).toString().equals(IngresarPalabra.getText())) {
                        System.out.println("La palabra ya existe");
                        palabraExiste = true;
                    }
                }
                if (palabraExiste == false) {
                    scroll1.setVisible(false);
                    String palabra = IngresarPalabra.getText();
                    String palabraT = IngresarTraduccion.getText();

                    try {
                        ArbolE.addPalabra(palabra, palabraT);
                        String[] fila = {
                            palabra,
                            palabraT};

                        if ("".equals(palabra)) {

                        } else {
                            modelo.addRow(fila);
                        }
                        repintar(ArbolE);

                    } catch (Exception error) {

                    }

                    IngresarPalabra.setText(null);
                    IngresarTraduccion.setText(null);
                } else {
                    JOptionPane.showMessageDialog(null, "La palabra ya existe.");
                }

            }

        });
        panel.add(Ingresar);
        panel.revalidate();
        panel.repaint();

        lPre = new JLabel("Pre: ");
        lPre.setBounds(0, 0, 500, 20);
        lPre.setVisible(true);
        info.add(lPre);
        lPos = new JLabel("POS: ");
        lPos.setBounds(0, 25, 500, 20);
        lPos.setVisible(false);
        info.add(lPos);
        lIn = new JLabel("IN: ");
        lIn.setBounds(0, 50, 500, 20);
        lIn.setVisible(false);
        info.add(lIn);

        inicio();

    }

    void createTable() {
        modelo.addColumn("Palabra");
        modelo.addColumn("Traduccion");

        scroll1.setBounds(750, 160, 500, 200);
        add(scroll1);
    }

    void inicio() {
        String palabra = IngresarPalabra.getText();
        String palabraT = IngresarTraduccion.getText();

        try {
            ArbolE.addPalabra(palabra, palabraT);

            repintar(ArbolE);
        } catch (Exception error) {
            System.out.println(error.toString());
        }
    }

    void filtrar(String palabre) {
        boolean ver = false;
        for (int i = 0; i < tabla.getRowCount(); i++) {
            if (tabla.getValueAt(i, 0).toString().equals(IngresarBuscar.getText())) {
                ver = true;
                break;
            }
        }
        if (ver) {
            tr = new TableRowSorter(modelo);
            tr.setRowFilter(RowFilter.regexFilter(IngresarBuscar.getText(), 0));
            tabla.setRowSorter(tr);
        } else {
            if (IngresarBuscar.getText() == "" | IngresarBuscar.getText().equals("") | IngresarBuscar.getText().equals(null)) {
                tr = new TableRowSorter(modelo);
                tr.setRowFilter(RowFilter.regexFilter(IngresarBuscar.getText(), 0));
                tabla.setRowSorter(tr);
            } else {
                JOptionPane.showMessageDialog(null, "La Palabra '" + IngresarBuscar.getText() + "' no existe");
            }
        }
    }

    public void repintar(logica arbol) {
        graphics = lienzo.getGraphics();
        lienzo.paint(graphics);
        g = (Graphics2D) graphics;
        paintArbolE(this.ArbolE.raiz, 10, 20);
        //lienzo.repaint();
    }

    private void paintArbolE(nodo node, int x, int y) {

        if (node != null) {
            g.setColor(Color.BLACK);
            g.setStroke(new BasicStroke(1));
            g.drawString(String.valueOf(node.getValor()), x, y);
            g.setColor(Color.RED);
            if (node.tieneHijos()) {
                g.drawLine(x + 3, y + 1, x + 3, y + 29);
                paintArbolE(node.getHijos().get(0), x, y + 41);

            } else {
                if (node.esFinal()) {
                    g.setColor(Color.BLACK);
                    g.drawString(String.valueOf(node.getPalabraTraducida()), x - 9, y + 12);
                    pintarhermanos(node, x, y - 41);
                }
            }
        }
    }

    public void pintarhermanos(nodo node, int x, int y) {
        if (node.getPadre().sigHermano() == null) {
            pintarhermanos(node.getPadre(), x, y - 41);
            return;
        }
        int ancho = node.getPadre().numNodoFinales();
        ancho = ancho - 1;
        g.setStroke(new BasicStroke(1));
        g.setColor(Color.RED);
        if (ancho == 0) {
            g.drawLine(x + 10, y - 6, x + (30), y - 6);
        } else {
            System.out.println(ancho);
            g.drawLine(x - (20 * ancho) - (10 * ancho), y - 6, x + (36), y - 6);
        }

        paintArbolE(node.getPadre().sigHermano(), x + 50, y);
    }
}