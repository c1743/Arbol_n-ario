package arbolnario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author AndresFWilT
 */
public class nodo {

    private char valor;
    private ArrayList<nodo> hijos;
    private String palabraTraducida;
    private nodo padre;
    private boolean f;

    nodo(char valor) {
        this.valor = Character.toLowerCase(valor);
        this.f = false;
        this.hijos = new ArrayList<nodo>();
        this.padre = null;
    }

    nodo(String palabraT) {
        this.hijos = new ArrayList<nodo>();
        this.valor = '}';
        this.palabraTraducida = palabraT;
        this.f = true;
        this.padre = null;
    }

    public int numNodoFinales() {
        int num = 0;
        if (this.f == true) {
            num += 1;
        } else {
            for (nodo nodo : this.hijos) {
                num += nodo.numNodoFinales();
            }
        }
        return num;
    }

    public boolean esFinal() {
        return this.f;
    }

    public String getPalabraTraducida() {
        return palabraTraducida;
    }

    public char getValor() {
        return valor;
    }

    public nodo hermanoMayor() {
        int i = this.padre.existe(this.valor);
        if (i > 0) {
            return this.padre.getHijos().get(i - 1);
        }
        return null;
    }

    public int profundidad() {
        int p = 0;
        p = this.hijos.size();
        System.out.println("Nodo:" + this.valor + " hijos:" + this.hijos.size());
        if (p != 0) {

        }
        for (nodo n : this.hijos) {
            p += n.profundidad();
        }
        return p;
    }

    public void setValor(char valor) {
        this.valor = Character.toLowerCase(valor);
        this.valor = valor;
    }

    public ArrayList<nodo> getHijos() {
        return hijos;
    }

    public nodo addHijo(char valorhijo) {
        if (Character.isLetter(valorhijo)) {
            nodo nuevo = new nodo(valorhijo);
            nuevo.setPadre(this);
            if (this.tieneHijos()) {
                int index = this.existe(valorhijo);
                if (index == -1) {
                    this.hijos.add(nuevo);
                    Collections.sort(this.hijos, nodo.valorComparator);
                    return nuevo;
                } else {
                    return this.hijos.get(index);
                }
            } else {
                this.hijos.add(nuevo);
                return nuevo;
            }
        }
        return this;
    }

    public nodo getPadre() {
        return padre;
    }

    public void setPadre(nodo padre) {
        this.padre = padre;
    }

    public boolean tieneHijos() {
        return !this.hijos.isEmpty();
    }

    public int hermanoMenor() {
        int i = this.getPadre().existe(this.valor);
        int lasti = this.getPadre().hijos.size() - 1;
        if (i == lasti) {
            return -1;
        }
        return i + 1;
    }

    public int numHermanos() {
        return padre.hijos.size() - 1;
    }

    public nodo sigHermano() {
        if (this.hermanoMenor() != -1) {
            return this.padre.hijos.get(this.hermanoMenor());
        }
        return null;

    }

    public void addFinal(String palabra) {
        nodo Ultimo = new nodo(palabra);
        Ultimo.setPadre(this);
        this.hijos.add(Ultimo);
    }

    public int existe(char valorhijo) {
        for (int i = 0; i < this.hijos.size(); i++) {
            if (hijos.get(i).getValor() == valorhijo) {
                return i;
            }
        }
        return -1;
    }

    public static Comparator<nodo> valorComparator = new Comparator<nodo>() {

        public int compare(nodo n1, nodo n2) {
            String valorNodo1 = String.valueOf(n1.getValor());
            String valorNodo2 = String.valueOf(n2.getValor());
            return valorNodo1.compareTo(valorNodo2);
        }
    };
}
